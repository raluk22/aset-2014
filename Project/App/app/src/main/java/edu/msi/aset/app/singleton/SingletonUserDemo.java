package edu.msi.aset.app.singleton;

public class SingletonUserDemo {

	public static void main(String[] args) {

		User object = User.getInstance();

	      object.sayHello();
	   }
}
