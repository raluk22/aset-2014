package qued.protocol;

import static org.junit.Assert.*;
import it.unisa.dia.gas.jpbc.Element;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import qued.protocol.edu.si.qed.HVEprotocol.algorithms.GenToken;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Query;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Setup;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.SpecificEncrypt;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.SpecificGenToken;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Utils;
import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.Ciphertext;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.HVEKey;
import qued.protocol.edu.si.qed.HVEprotocol.models.SubsetAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.Token;

public class EncryptQueryTest {

	private Setup setup;
	
	private SpecificEncrypt encryption;
	
	private GenToken genToken;
	
	@Before
	public void schemeFlow() {
		BigInteger lambda = new BigInteger(100, (new Random()));
		
		setup = new Setup(lambda, 124);
		
		encryption = new SpecificEncrypt();
		
		genToken = new SpecificGenToken();
	}
	
	@Test
	public void correctnessOfQuery() {
		int l = 10;
		HVEKey key = setup.genKey("guest1", l); 
		
		//Create a list of attributes
		ArrayList<BigInteger> attributes = new ArrayList<BigInteger>(l);
		for (int i=0; i<l; i++) {
			attributes.add(Utils.fromStringToBigInteger("attribute_"+i));
		}
				
		AttributeList _I = new EqAttributeList(l, attributes);
				
		Element initialMessage = key.getPK().getGroupGT().newRandomElement();
		
		//Create a list of attributes I*
		int[] index = new int[2];
		index[0] = 0;
		index[1] = 1;
		
		ArrayList<BigInteger> attributes_ = new ArrayList<BigInteger>(l);
		for (int i=0; i<l; i++) {
			attributes_.add(Utils.fromStringToBigInteger(""));
		}
		attributes_.set(index[0], Utils.fromStringToBigInteger("attribute_6"));
		attributes_.set(index[1], Utils.fromStringToBigInteger("attribute_1"));
		

		AttributeList _I_ = new EqAttributeList(l, attributes_);	
		
		Ciphertext cipher = encryption.encrypt(key.getPK(), _I, initialMessage);
		Token tK = genToken.genToken(key.getSK(), _I_, index, setup.getParameters().getBigInteger("n0"));
		
		//Query
		Element returnedMessage = Query.query(tK, cipher, setup.getE());

		
		Element test = returnedMessage.pow(setup.getGroupGT().getOrder());
		assertEquals(test, setup.getGroupGT().newOneElement());
		
	}
	
	@Test
	public void correctnessOfEncryptQueryTestForEquality() {
		int l = 10;
		HVEKey key = setup.genKey("guest1", l); 
		
		//Create a list of attributes
		ArrayList<BigInteger> attributes = new ArrayList<BigInteger>(l);
		for (int i=0; i<l; i++) {
			attributes.add(Utils.fromStringToBigInteger("attribute_"+i));
		}
				
		AttributeList _I = new EqAttributeList(l, attributes);
				
		Element initialMessage = key.getPK().getGroupGT().newRandomElement();
		
		//Create a list of attributes I*
		int[] index = new int[2];
		index[0] = 0;
		index[1] = 1;
		
		ArrayList<BigInteger> attributes_ = new ArrayList<BigInteger>(l);
		for (int i=0; i<l; i++) {
			attributes_.add(Utils.fromStringToBigInteger(""));
		}
		attributes_.set(index[0], Utils.fromStringToBigInteger("attribute_0"));
		attributes_.set(index[1], Utils.fromStringToBigInteger("attribute_1"));
		

		AttributeList _I_ = new EqAttributeList(l, attributes_);	
		
		Ciphertext cipher = encryption.encrypt(key.getPK(), _I, initialMessage);
		Token tK = genToken.genToken(key.getSK(), _I_, index, setup.getParameters().getBigInteger("n0"));
		
		//Query
		Element returnedMessage = Query.query(tK, cipher, setup.getE());

		assertEquals(initialMessage, returnedMessage);
		
	}
	
	@Test
	public void correctnessOfEncryptQueryTestForSubset() {
		int l = 2;
		
		//Create a list of attributes
		ArrayList<Set<BigInteger>> attributes = new ArrayList<Set<BigInteger>>(l);
		for (int i=0; i<l; i++) {
			Set set = new HashSet<BigInteger>();
			set.add(Utils.fromStringToBigInteger("attribute_"+i));
			attributes.add(set);
		}
				
		Set<BigInteger> alphabat = new HashSet<BigInteger>(l);
		for (int i=0; i<l; i++) {
			alphabat.add(Utils.fromStringToBigInteger("attribute_"+i));
		}
		
		AttributeList _I = new SubsetAttributeList(l, attributes, alphabat);
		HVEKey key = setup.genKey("guest1", l*alphabat.size()); 
		
		Element initialMessage = key.getPK().getGroupGT().newRandomElement();
		
		//Create a list of attributes I*
		int[] index = new int[2];
		index[0] = 0;
		index[1] = 1;
		
		ArrayList<Set<BigInteger>> attributes_ = new ArrayList<Set<BigInteger>>(2);
		for (int i=0; i<l; i++) {
			attributes_.add(new HashSet<BigInteger>());
		}
		
		Set<BigInteger> A1 = new HashSet<BigInteger>();
		A1.add(Utils.fromStringToBigInteger("attribute_0"));
		A1.add(Utils.fromStringToBigInteger("attribute_5"));
		A1.add(Utils.fromStringToBigInteger("attribute_6"));
		Set<BigInteger> A2 = new HashSet<BigInteger>();
		A2.add(Utils.fromStringToBigInteger("attribute_0"));
		A2.add(Utils.fromStringToBigInteger("attribute_1"));
		
		attributes_.set(index[0], A1);
		attributes_.set(index[1], A2);
		AttributeList _I_ = new SubsetAttributeList(l, attributes_, alphabat);	
		
		Ciphertext cipher = encryption.encrypt(key.getPK(), _I, initialMessage);
		Token tK = genToken.genToken(key.getSK(), _I_, index, setup.getParameters().getBigInteger("n0"));
		
		//Query
		Element returnedMessage = Query.query(tK, cipher, setup.getE());

		assertEquals(initialMessage, returnedMessage);
		
	}
	
	@Test
	public void differentSKTest() {
		int l = 10;
		HVEKey key = setup.genKey("guest1", l); 
		HVEKey key2 = setup.genKey("guest2", l);
		
		//Create a list of attributes
		ArrayList<BigInteger> attributes = new ArrayList<BigInteger>(l);
		for (int i=0; i<l; i++) {
			attributes.add(Utils.fromStringToBigInteger("attribute_"+i));
		}
				
		AttributeList _I = new EqAttributeList(l, attributes);
				
		Element initialMessage = key.getPK().getGroupGT().newRandomElement();
		
		//Create a list of attributes I*
		int[] index = new int[2];
		index[0] = 0;
		index[1] = 1;
		
		ArrayList<BigInteger> attributes_ = new ArrayList<BigInteger>(l);
		for (int i=0; i<l; i++) {
			attributes_.add(Utils.fromStringToBigInteger(""));
		}
		attributes_.set(index[0], Utils.fromStringToBigInteger("attribute_0"));
		attributes_.set(index[1], Utils.fromStringToBigInteger("attribute_1"));
		

		AttributeList _I_ = new EqAttributeList(l, attributes_);	
		
		Ciphertext cipher = encryption.encrypt(key.getPK(), _I, initialMessage);
		Token tK = genToken.genToken(key2.getSK(), _I_, index, setup.getParameters().getBigInteger("n0"));
		
		//Query
		Element returnedMessage = Query.query(tK, cipher, setup.getE());

		assertFalse(initialMessage.equals(returnedMessage));
	}
	
	@Test
	public void notMatchedAttributeListTest() {
		int l = 10;
		HVEKey key = setup.genKey("guest1", l); 
		
		//Create a list of attributes
		ArrayList<BigInteger> attributes = new ArrayList<BigInteger>(l);
		for (int i=0; i<l; i++) {
			attributes.add(Utils.fromStringToBigInteger("attribute_"+i));
		}
				
		AttributeList _I = new EqAttributeList(l, attributes);
				
		Element initialMessage = key.getPK().getGroupGT().newRandomElement();
		
		//Create a list of attributes I*
		int[] index = new int[2];
		index[0] = 0;
		index[1] = 1;
		
		ArrayList<BigInteger> attributes_ = new ArrayList<BigInteger>(l);
		for (int i=0; i<l; i++) {
			attributes_.add(Utils.fromStringToBigInteger(""));
		}
		attributes_.set(index[0], Utils.fromStringToBigInteger("attribute_0"));
		attributes_.set(index[1], Utils.fromStringToBigInteger("attribute_5"));
		

		AttributeList _I_ = new EqAttributeList(l, attributes_);	
		
		Ciphertext cipher = encryption.encrypt(key.getPK(), _I, initialMessage);
		Token tK = genToken.genToken(key.getSK(), _I_, index, setup.getParameters().getBigInteger("n0"));
		
		//Query
		Element returnedMessage = Query.query(tK, cipher, setup.getE());

		assertFalse(initialMessage.equals(returnedMessage));
	}
}
