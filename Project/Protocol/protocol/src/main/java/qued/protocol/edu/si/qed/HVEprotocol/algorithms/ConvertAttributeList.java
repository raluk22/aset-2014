package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import java.util.ArrayList;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;

public interface ConvertAttributeList {

	public void convert(AttributeList initial_format, ArrayList<Integer> index, EqAttributeList final_format) throws Exception;
}
