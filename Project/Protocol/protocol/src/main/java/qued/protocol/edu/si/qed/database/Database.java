package qued.protocol.edu.si.qed.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import qued.protocol.edu.si.qed.HVEprotocol.models.Ciphertext;
import qued.protocol.edu.si.qed.models.TransactionEntity;

public class Database {

	public static Connection getConnection(String database, String username, String password) {
		Connection c = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/"+database, username, password);
		    c.setAutoCommit(false);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
		return c;
	}
	
	public static List<Ciphertext> getCryptedTransactions(Connection c) {
		ArrayList<Ciphertext> results = new ArrayList<Ciphertext>();
		
		Statement stmt;
		try {
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM TransactionCrypted;" );
			
			while (rs.next()) {
				String ciphertext = rs.getString("ciphertext");
				results.add(Ciphertext.readObject(ciphertext));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public static List<TransactionEntity> getTransactions(Connection c) {
		ArrayList<TransactionEntity> results = new ArrayList<TransactionEntity>();
		
		Statement stmt;
		try {
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM Transaction;" );
			
			while (rs.next()) {
				 String senderId = String.valueOf(rs.getInt("SenderId"));
				 String senderName = rs.getString("SenderName");
				 String beneficiaryID = String.valueOf(rs.getInt("BeneficiaryId"));;
				 String beneficiaryName = rs.getString("BeneficiaryName");
				 String beneficiaryIBAN = rs.getString("IBAN");
			     Double amount = rs.getDouble("Amount");
				 String currency = rs.getString("Currency");
				 Date transactionDate = rs.getDate("Date");
				 String description = rs.getString("Description");

				results.add(new TransactionEntity(senderId, senderName, beneficiaryID, beneficiaryName, beneficiaryIBAN, amount, currency, transactionDate, description));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public static void addCryptedTransaction(String ciphertext, Connection c) {
		Statement stmt = null;
		 
		 try {
			stmt = c.createStatement();
			
			String query = "INSERT INTO TransactionCrypted ciphertext "
					+ "VALUES ('"+ ciphertext + "');";
			
			stmt.executeUpdate(query); 
			c.commit();
			stmt.close();
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void addTransaction(TransactionEntity t, Connection c) {
		Statement stmt = null;
		 
		 try {
			stmt = c.createStatement();
			
			String query = "INSERT INTO Transaction (SenderId, SenderName, BeneficiaryId, BeneficiaryName, Amount, Currency, Description, Date) "
					+ "VALUES ('"+ t.getSenderId()+", "+ t.getSenderName()+ ", "+ t.getBeneficiaryID()+ ", "+
					t.getBeneficiaryIBAN()+ ", "+ t.getAmount()+ ", "+ t.getCurrency()+ ", "+ t.getDescription()+ ", "+ t.getTransactionDate()+ "');";
			
			stmt.executeUpdate(query); 
			c.commit();
			stmt.close();
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
