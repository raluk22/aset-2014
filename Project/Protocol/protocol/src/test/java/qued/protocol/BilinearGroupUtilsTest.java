package qued.protocol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Field;
import it.unisa.dia.gas.jpbc.Pairing;
import it.unisa.dia.gas.jpbc.PairingParameters;
import it.unisa.dia.gas.jpbc.PairingParametersGenerator;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;
import it.unisa.dia.gas.plaf.jpbc.pairing.a1.TypeA1CurveGenerator;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import qued.protocol.edu.si.qed.HVEprotocol.algorithms.BilinearGroupUtils;

public class BilinearGroupUtilsTest {

	private PairingParameters parameters;

	private Pairing e;

	private Field groupG;

	private Field groupGT;



	@Before
	public void setup() {
		PairingParametersGenerator<?> parametersGenerator = new TypeA1CurveGenerator(
				2, 10);

		parameters = parametersGenerator.generate();

		e = PairingFactory.getPairing(parameters);

		groupG = e.getG1();
		groupGT = e.getGT();
	}

	@Test
	public void getGeneratorForGpTestIsNotNull() {
		Element gp = BilinearGroupUtils.getGeneratorForGp(groupG, parameters);
		assertTrue(gp != null);
	}

	@Test
	public void getGeneratorForGqTestIsNotNull() {
		Element gq = BilinearGroupUtils.getGeneratorForGq(groupG, parameters);
		assertTrue(gq != null);
	}

	@Test
	public void getGeneratorForGpTestIsOfCorrectOrder() {
		Element gq = BilinearGroupUtils.getGeneratorForGp(groupG, parameters);
		
		Element test = gq.pow(parameters.getBigInteger("n0"));
		assertEquals(new BigInteger("0"),test.toBigInteger());
	}

	@Test
	public void getRandomFromSubgroupOfGTestIsOfCorrectOrder() {
		Element gen = BilinearGroupUtils.getGeneratorForGp(groupG, parameters);
		Element g = BilinearGroupUtils.getRandomFromSubgroupOfG(gen, parameters);
		assertEquals(gen.getLengthInBytes(),g.getLengthInBytes());
	}
	
}
