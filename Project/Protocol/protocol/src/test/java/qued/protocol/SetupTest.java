package qued.protocol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import it.unisa.dia.gas.jpbc.PairingParameters;

import java.math.BigInteger;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Setup;

public class SetupTest {

	private Setup setup;

	@Before
	public void setup() {
		BigInteger lambda = new BigInteger(100, (new Random()));
		
		setup = new Setup(lambda, 124);
	}

	@Test
	public void initializerTest() {
		PairingParameters p = setup.getParameters();
		assertEquals(p.getBigInteger("n"), p.getBigInteger("n0").multiply(p.getBigInteger("n1")));
	}
	
	@Test
	public void pIsPrime() {
		PairingParameters p = setup.getParameters();
		assertTrue(p.getBigInteger("n0").isProbablePrime(1));
	}
	

	@Test
	public void qIsPrime() {
		PairingParameters q = setup.getParameters();
		assertTrue(q.getBigInteger("n1").isProbablePrime(1));
	}
	
	@Test 
	public void keyIsNotNull() {
		assertNotNull(setup.genKey("test", 10));
	}
	
	@Test
	public void testGetKey() {
		setup.genKey("test", 10);
		assertNotNull(setup.getKey("test"));
	}
	
}
