package qued.protocol.edu.si.qed.HVEprotocol.models;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Set;

public class SubsetAttributeList implements AttributeList{

	private Integer l;
	private ArrayList<Set<BigInteger>> values;
	private Set<BigInteger> T;
	
	public SubsetAttributeList() {}
	
	public SubsetAttributeList(Integer l, ArrayList<Set<BigInteger>> values, Set<BigInteger> T) {
		this.l = l;
		this.values = values;
		this.T = T;
	}
	
	public Integer getL() {
		l = values.size();
		return l;
	}

	public ArrayList<BigInteger> getValues() {
		return convert();
	}
	
	public ArrayList<Set<BigInteger>> getValuesSet() {
		return values;
	}
	
	public Set<BigInteger> getT() {
		return T;
	}

	public ArrayList<BigInteger> convert() {
		return null;
	}

	public void setL(Integer l) {
		this.l = l;	
	}

	public void setValues(ArrayList<BigInteger> values) {
	}

}
