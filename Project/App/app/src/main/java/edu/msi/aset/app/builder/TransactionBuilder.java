package edu.msi.aset.app.builder;

import java.util.Date;

import edu.msi.aset.app.model.BeneficiaryEntity;
import edu.msi.aset.app.model.TransactionEntity;
import edu.msi.aset.app.model.UserEntity;

public class TransactionBuilder {

	private TransactionEntity transaction;
	
	public TransactionBuilder(){
		 this.transaction = new TransactionEntity();
	}

	public TransactionBuilder withAmount(Double amount) {
		this.transaction.setAmount(amount);
		return this;
	}
	public TransactionBuilder withTransactionDate(Date transactionDate) {
		this.transaction.setTransactionDate(transactionDate);
		return this;
	}
	public TransactionBuilder withBeneficiary(BeneficiaryEntity beneficiary) {
		this.transaction.setBeneficiary(beneficiary);
		return this;
	}
	public TransactionBuilder withDescription(String description) {
		this.transaction.setDescription(description);
		return this;
	}
	public TransactionBuilder withPayee(UserEntity payee) {
		this.transaction.setPayee(payee);
		return this;
	}

	public TransactionEntity build() {
		return this.transaction;
	}
	
}
