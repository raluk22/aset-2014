package edu.msi.aset.app.dao.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.msi.aset.app.dao.UserDAO;
import edu.msi.aset.app.model.UserEntity;

@Service
@Transactional
public class UserDAOImpl extends AbstractDAOImpl<UserEntity> implements UserDAO {

	protected UserDAOImpl(Class<UserEntity> entityClass) {
		super(entityClass);
	}
	
	public UserDAOImpl() {
		super(UserEntity.class);
	}

}