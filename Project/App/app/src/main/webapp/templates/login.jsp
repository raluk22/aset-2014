<ion-view title>
    <ion-content has-header="true" padding="true">
        <form class="list">
            <label class="item item-input">
                <input type="text" placeholder="Username">
            </label>
            <label class="item item-input">
                <input type="password" placeholder="Password">
            </label>
            <div class="padding">
                <button class="button button-block button-positive" ng-click="login()">Login</button>
            </div>
        </form>
    </ion-content>
</ion-view>
