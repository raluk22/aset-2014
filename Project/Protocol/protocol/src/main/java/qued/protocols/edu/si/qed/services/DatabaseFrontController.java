package qued.protocols.edu.si.qed.services;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Pairing;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Query;
import qued.protocol.edu.si.qed.HVEprotocol.models.Ciphertext;
import qued.protocol.edu.si.qed.HVEprotocol.models.Token;
import qued.protocol.edu.si.qed.database.Database;
import qued.protocol.edu.si.qed.models.TransactionEntity;

public class DatabaseFrontController {
	
	public List<Element> query(Token tK, Pairing e, TrustedAuthority ta) {
		List<Element> results = new ArrayList<Element>();
		
		Connection conn = Database.getConnection("ASET2014", "admin", "1234");
		List<Ciphertext> ciphers = Database.getCryptedTransactions(conn);
		
		for (Ciphertext cipher: ciphers) {
			Element mess = Query.query(tK, cipher, e);
			if (ta.isMInMessageSpace(mess))
				results.add(mess);
		}
		
		return results;
	}
	
	public void makeTransaction(TransactionEntity t, String cipher) {
		Connection c = Database.getConnection("ASET2014", "admin", "1234");
		Database.addTransaction(t, c);
		Database.addCryptedTransaction(cipher, c);
	}
}
