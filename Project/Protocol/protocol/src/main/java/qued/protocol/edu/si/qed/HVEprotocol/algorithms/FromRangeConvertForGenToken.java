package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import java.math.BigInteger;
import java.util.ArrayList;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;

public class FromRangeConvertForGenToken implements ConvertAttributeList {

    private Integer n;

    public Integer getN() {
        return n;
    }

    public FromRangeConvertForGenToken() {
    }

    public FromRangeConvertForGenToken(Integer n) {
        this.n = n;
    }
    
    public void convert(AttributeList initialFormat, ArrayList<Integer> indexVector,
            EqAttributeList finalFormat) throws Exception {

        Integer w = initialFormat.getL();
        Integer n = getN();
        finalFormat.setL(w * n);
        
        ArrayList<BigInteger> S = initialFormat.getValues();
        ArrayList<BigInteger> sigmaS = new ArrayList<BigInteger>();
        ArrayList<Integer> index = new ArrayList<Integer>();
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < n; j++) {
                BigInteger xi = S.get(i);
                if (xi.equals(BigInteger.valueOf(j))) {
                    sigmaS.add(BigInteger.ONE);
                    index.add(j);
                } else {
                    sigmaS.add(null);
                }
            }
        }       
        finalFormat.setValues(sigmaS);
    }
}
