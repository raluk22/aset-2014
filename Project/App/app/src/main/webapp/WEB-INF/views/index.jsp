<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Form</title>

        <link href="${pageContext.request.contextPath}/resources/css/ionic.css" rel="stylesheet">

        <script src="${pageContext.request.contextPath}/resources/js/ionic.bundle.js"></script>  
        
        <script src="${pageContext.request.contextPath}/resources/app/app.js"></script>    
        <script src="${pageContext.request.contextPath}/resources/app/controller.js"></script>          
    </head>
    
    <body ng-app="starter" animation="slide-left-right-ios7">
        <ion-nav-bar class="bar-stable nav-title-slide-ios7">
          <ion-nav-back-button class="button-icon icon  ion-ios7-arrow-back">
            Back
          </ion-nav-back-button>
        </ion-nav-bar>
        <ion-nav-view></ion-nav-view>
  </body>
</html>
