package edu.msi.aset.app.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import edu.msi.aset.app.service.ProxyUserService;

@Controller
public class RegisterController {
	
	@Autowired
	private ProxyUserService userService;

	@RequestMapping(value = { "/doRegister" })
	public ModelAndView doRegister(HttpSession session,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		return model;
	}
}