package edu.msi.aset.app.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;

public interface AbstractDAO<E> {

    E findById(int entityId);

    List<E> findListByCriteria(Criterion criterion);

    List<E> findAll();

    E findByCriteria(Criterion criterion);

    void save(E entityToBeSaved);

    void update(E entityToBeUpdated);

    void remove(E entityToBeRemoved);

    E remove(int entityToBeRemovedId);

}