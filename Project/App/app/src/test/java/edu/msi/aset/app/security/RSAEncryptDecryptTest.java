
package edu.msi.aset.app.security;

import java.io.UnsupportedEncodingException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alice
 */
public class RSAEncryptDecryptTest {
    
    public RSAEncryptDecryptTest() {
    }
    
    /**
     * Test of encrypt method, of class RSAEncryptDecrypt.
    */
    
    @Test
    public void testEncryptLength() throws UnsupportedEncodingException{      
        int expectedLength = 1024; 
        String plainText = "plainText";
        
        RSAEncryptDecrypt rsaed = new RSAEncryptDecrypt();
        String criptotext = rsaed.encrypt(plainText);
        
        byte[] criptotextLenght = criptotext.getBytes("UTF-8");

        assertEquals(criptotextLenght, expectedLength);
    }
    @Test
    public void testEncrypt() throws UnsupportedEncodingException{       
        String text1 = "text1";
        String text2 = "text2MaiLung";
        
        RSAEncryptDecrypt rsaed = new RSAEncryptDecrypt();
        String criptotext1 = rsaed.encrypt(text1);
        String criptotext2 = rsaed.encrypt(text2);
        
        byte[] criptotext1Bytes = criptotext1.getBytes("UTF-8");
        byte[] criptotext2Bytes = criptotext2.getBytes("UTF-8");

        assertEquals(criptotext1Bytes, criptotext2Bytes);    
     }
    
    
    /**
     * Test of decrypt method, of class RSAEncryptDecrypt.
     */
    @Test
    public void testDecrypt() {
        String plaintext = "text1";
        RSAEncryptDecrypt rsaed = new RSAEncryptDecrypt();
        String encrypt = rsaed.encrypt(plaintext);        
        String decrypt = rsaed.decrypt(encrypt);
        
        assertEquals("text1", decrypt);
    }
    
}
