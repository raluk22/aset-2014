package edu.msi.aset.app.dao;

import edu.msi.aset.app.model.UserEntity;


public interface UserDAO extends AbstractDAO<UserEntity> {
}