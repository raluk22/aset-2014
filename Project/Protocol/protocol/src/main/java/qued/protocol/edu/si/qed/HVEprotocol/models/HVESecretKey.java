package qued.protocol.edu.si.qed.HVEprotocol.models;

import java.math.BigInteger;

import it.unisa.dia.gas.jpbc.Element;

public class HVESecretKey {

	  private Element[][] theUHWMatrix;
	
	  private Element g;
	
	  private Element v;
	
	  private Element g_q;
	  
	  private BigInteger alpha;
	
	public Element[][] getTheUHWMatrix() {
		return theUHWMatrix;
	}
	
	public Element getG() {
		return g;
	}
	
	public Element getV() {
		return v;
	}
	
	public Element getG_q() {
		return g_q;
	}
	
	public BigInteger getAlpha() {
		return alpha;
	}

	public HVESecretKey(Element[][] theUHWMatrix, Element g, Element v,
			Element g_q, BigInteger alpha) {
		super();
		this.theUHWMatrix = theUHWMatrix;
		this.g = g;
		this.v = v;
		this.g_q = g_q;
		this.alpha = alpha;
	}
}