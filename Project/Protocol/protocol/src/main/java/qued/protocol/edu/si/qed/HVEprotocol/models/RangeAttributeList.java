package qued.protocol.edu.si.qed.HVEprotocol.models;

import java.math.BigInteger;
import java.util.ArrayList;

public class RangeAttributeList implements AttributeList{
	private Integer l;
	private ArrayList<BigInteger> values = new ArrayList<BigInteger>();
	private int[] index;
        
	public RangeAttributeList() {}
	
	public RangeAttributeList(Integer l, ArrayList<BigInteger> values) {
		this.l = l;
                for (BigInteger item : values)         
                	this.values.add(item);
	}

	public Integer getL() {
		return l;
	}

	public ArrayList<BigInteger> getValues() {
		return values;
	}
        public int[] getIndex(){
            return index;
        }
        
         public void setL(Integer l){
            this.l = l;                
        }

         public void setValues(ArrayList<BigInteger> values){
            for (BigInteger item : values) {
                this.values.add(item);
            }
         }
         
         public void setIndex(int[] index){ 
             for(int i=0; i< index.length; i++)
                    this.index[i] = index[i];
         }
        
}
