package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import java.math.BigInteger;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Field;

public class Utils {

	//Maps a string to an element of the specified group
	public static Element fromStringToElement(Field group, String values) {
		Element res = group.newRandomElement();
		
		BigInteger pow = BigInteger.valueOf(values.hashCode()).mod(group.getOrder());
		res = res.pow(pow);
		
		return res;
	}
	
	//Maps a string to a BigInteger by using the hash code
	public static BigInteger fromStringToBigInteger(String values) {
		BigInteger res = BigInteger.valueOf(values.hashCode());
		return res;
	}
}
