package qued.protocol;

import it.unisa.dia.gas.jpbc.Element;

import java.math.BigInteger;
import java.sql.Connection;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;












import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Encrypt;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Encrypt_refactor;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.GenToken;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Query;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Setup;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.SpecificEncrypt;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.SpecificGenToken;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Utils;
//import qued.protocol.edu.si.qed.HVEprotocol.Encrypt;
import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.Ciphertext;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.HVEKey;
import qued.protocol.edu.si.qed.HVEprotocol.models.Token;
import qued.protocol.edu.si.qed.database.Database;
import qued.protocol.edu.si.qed.models.TransactionEntity;
import qued.protocols.edu.si.qed.services.DatabaseFrontController;
import qued.protocols.edu.si.qed.services.TrustedAuthority;

public class App {

    public static void main(String[] args) throws Exception {
    	
    	TrustedAuthority ta = new TrustedAuthority();
    	DatabaseFrontController dbController = new DatabaseFrontController();

    	int l = 3;
		HVEKey key = ta.generateNewKey("guest1", l); 
		
		//Transaction 1
		TransactionEntity t = new TransactionEntity("1","Popescu Ion","5","Vasilache Ioana", "GR16 0110 1250 0000 0001 2300 695", new Double("1500.0"), "RON", new Date(), "Order payment.");
    	makeTransaction(t, key, dbController, ta);
    	
    	//Transaction 2
    	t = new TransactionEntity("2","Gicu Alex","5","Gicu Ciprian", "GR16 0110 1250 0000 0001 5678 809", new Double("1900.0"), "RON", new Date(), "Order payment.");
    	makeTransaction(t, key, dbController, ta);
    	
    	//Transaction 3
    	t = new TransactionEntity("3","Gicu Alex","5","Unilatcu Florin", "IT40 S054 2811 1010 0000 0123 456 ", new Double("1900.0"), "EUR", new Date(), "Money transfer.");
    	makeTransaction(t, key, dbController, ta);
	
		//Create a list of attributes I*
		int[] index = new int[1];
		index[0] = 1;
		//index[1] = 1;
		
		ArrayList<BigInteger> attributes_ = new ArrayList<BigInteger>(l);
		for (int i=0; i<l; i++) {
			attributes_.add(Utils.fromStringToBigInteger(""));
		}
		//attributes_.set(index[0], Utils.fromStringToBigInteger("1500.0"));
		attributes_.set(index[0], Utils.fromStringToBigInteger("RON"));
		
		AttributeList _I_ = new EqAttributeList(l, attributes_);	
		
		//Generate token
		Token tK = ta.generateToken(key.getSK(), _I_, index);
		
		//Query
		List<Element> messages = dbController.query(tK, key.getPK().getE(), ta);
		
		System.out.println("The results of the equality query:");
		for (Element m: messages) {
			System.out.println(">> "+ ta.mapToString(m));
		}
	    }

    public static void makeTransaction(TransactionEntity t, HVEKey key, DatabaseFrontController dbController, TrustedAuthority ta) {
    	
    	//Create a list of attributes
    		ArrayList<BigInteger> attributes = new ArrayList<BigInteger>(3);
    		attributes.add(Utils.fromStringToBigInteger(t.getAmount().toString()));
    		attributes.add(Utils.fromStringToBigInteger(t.getTransactionDate().toString()));
    		attributes.add(Utils.fromStringToBigInteger(t.getCurrency()));
    			
    		AttributeList _I = new EqAttributeList(3, attributes);
    			
    	//Encrypt the information
    		Encrypt enc = new SpecificEncrypt();
    		Ciphertext cipher = enc.encrypt(key.getPK(), _I, ta.mapToPoint(t.toString()));
    	
    	// Put them into the database
    		dbController.makeTransaction(t, Ciphertext.writeObject(cipher));
    }
}
