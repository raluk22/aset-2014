package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.SubsetAttributeList;


public class FromSubsetConvertForEncrypt implements ConvertAttributeList{

	public void convert(AttributeList initial_format, ArrayList<Integer> index,
			EqAttributeList final_format) throws Exception {
		
		ArrayList<Set<BigInteger>> values = ((SubsetAttributeList) initial_format).getValuesSet();
		int i=0;
		int n;
		
		for (Set<BigInteger> set: values) {
			if (set.size()==1) {
				Iterator<BigInteger> it = set.iterator();
				
				BigInteger x = it.next();
				
				Set<BigInteger> alphabet = ((SubsetAttributeList) initial_format).getT();
				n = alphabet.size();
				
				if (alphabet.contains(x)) {
					int j =0;
					
					for (BigInteger el: alphabet) {
						if (el.equals(x)) {
							final_format.getValues().add(i*n+j, new BigInteger("0"));
						}
						else 
						{
							final_format.getValues().add(i*n+j, new BigInteger("1"));
						}
						j++;
					}
				}
			}
			
			i++;
		}
	}

}
