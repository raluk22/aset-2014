package edu.msi.aset.app.factory;

public class CardFactory {

	public Card getType(String type) {
		if ("credit".equals(type)) {
			return new CreditCard();
		} else {
			return new DebitCard();
		}
	}
	
}
