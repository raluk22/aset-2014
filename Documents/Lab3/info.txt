________________________________________________________________________________________
Members' activity
________________________________________________________________________________________

Burdujanu G. Alice-Georgiana:	Activity & State Diagrams

Cojocaru M. Iulia-Andreea:	Activity & State Diagrams

Damian V. Lavinia Mariana:	Package & Class Diagrams for the HVE Protocol

Gimbuta M. Amalia-Raluca:	Package & Class Diagrams for the Application
				Deployment Diagram

Lazarescu D. Andrei Daniel:	-

Pavel E. Costel-Sorin:		Use-case & Sequence Diagrams


For the Requirements Analysis task, the members of the team met up in order to discuss 
the aspects of the application.