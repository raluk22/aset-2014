package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Set;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;

public class FromRangeConvertForEncrypt implements ConvertAttributeList {

    private Integer n;

    public Integer getN() {
        return n;
    }

    public FromRangeConvertForEncrypt() {
    }

    public FromRangeConvertForEncrypt(Integer n) {
        this.n = n;
    }

    public void convert(AttributeList initialFormat,
            EqAttributeList finalFormat) {
        //Building the vector sigma(S) = (sigma_i,j)
        Integer w = initialFormat.getL();
        Integer n = getN();
        finalFormat.setL(w * n);
        ArrayList<BigInteger> S = initialFormat.getValues();

        ArrayList<BigInteger> sigmaS = new ArrayList<BigInteger>();
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < n; j++) {
                BigInteger xi = S.get(i);
                if (xi.compareTo(BigInteger.valueOf(j)) > 0 || xi.equals(BigInteger.valueOf(j))) {
                    sigmaS.add(BigInteger.ONE);
                } else {
                    sigmaS.add(BigInteger.ZERO);
                }
            }
        }

        finalFormat.setValues(sigmaS);

    }

    public void convert(AttributeList initial_format, ArrayList<Integer> index, EqAttributeList final_format) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
