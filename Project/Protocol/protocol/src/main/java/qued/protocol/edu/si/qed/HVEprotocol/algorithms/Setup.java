package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Field;
import it.unisa.dia.gas.jpbc.Pairing;
import it.unisa.dia.gas.jpbc.PairingParameters;
import it.unisa.dia.gas.jpbc.PairingParametersGenerator;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;
import it.unisa.dia.gas.plaf.jpbc.pairing.a1.TypeA1CurveGenerator;

import java.math.BigInteger;
import java.nio.channels.UnsupportedAddressTypeException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import qued.protocol.edu.si.qed.HVEprotocol.models.HVEKey;
import qued.protocol.edu.si.qed.HVEprotocol.models.HVEPublicKey;
import qued.protocol.edu.si.qed.HVEprotocol.models.HVESecretKey;


public class Setup {

  private PairingParameters parameters;

  private Pairing e;
  
  private Field groupG;
  
  private Field groupGT;

  private BigInteger lambda;
  
  private Integer l;

  private Map<String, HVEKey> usersKeys;

  private Set<Element> messageSpaceM;

  public Setup(BigInteger newLambda) {
	  lambda = newLambda;
  }
  
  public Setup(BigInteger newLambda, Integer numberOfBits) {
	  lambda = newLambda;
	  initialize(numberOfBits);
	  usersKeys = new HashMap<String, HVEKey>();
	  messageSpaceM = new HashSet<Element>();
  }

  private void initialize(Integer numberOfBits) {
	  
	// JPBC Type A1 pairing generator...
		  PairingParametersGenerator<?> parametersGenerator = new TypeA1CurveGenerator(
		      2,  // the number of primes
		      numberOfBits // the bit length of each prime
		  );

    // Generating the parameters for the pairing
		  parameters = parametersGenerator.generate();
		 
    // Generating the bilinear application e
		  e = PairingFactory.getPairing(parameters); 
		  groupG = e.getG1();
		  groupGT = e.getGT();
  }
  
  public HVEKey genKey(String userID, Integer newL) {
	  l = newL;
			
	//Getting a generator for subgroup Gp ...
	  Element g_p = BilinearGroupUtils.getGeneratorForGp(groupG, parameters);
	  
	  //... and a generator for subgroup Gq
	  Element g_q = BilinearGroupUtils.getGeneratorForGq(groupG, parameters);
			  
	  //Generating random (u_i, h_i, w_i) triplets for i in {1..l}, u_i, h_i, w_i are elements in Gp
	  Element listOfUHWTriplets[][] = BilinearGroupUtils.generateTriplets(l.intValue(), g_p, parameters);
	  
	  //Choosing random g and v from Gp
	  Element g = BilinearGroupUtils.getRandomFromSubgroupOfG(g_p, parameters);
	  Element v = BilinearGroupUtils.getRandomFromSubgroupOfG(g_p, parameters);
	 
	  //Choosing random alpha from Zp
	  BigInteger alpha = new BigInteger(parameters.getBigInteger("n0").bitLength(), new Random()).mod(parameters.getBigInteger("n0"));
	  
	  //Creating the secret key
	  HVESecretKey sk = new HVESecretKey(listOfUHWTriplets, g, v, g_q, alpha);
	  
	  //Choosing random blinding factors from Gq
	  Element listOfRTriplets[][] = BilinearGroupUtils.generateTriplets(l.intValue(), g_q, parameters);
	  Element r_v = BilinearGroupUtils.getRandomFromSubgroupOfG(g_q, parameters);
	 
	  //Computing V
	  Element _V = v.duplicate().mul(r_v);
	  
	  //Computing A
	  Element _A = e.pairing(g, v).pow(alpha);
	 
	  //Computing the matrix UHW
	  Element  theUHWMatrix[][] = computeUHWMatrix(listOfUHWTriplets, listOfRTriplets, l.intValue());
	  
	  //Creating the public key
	  HVEPublicKey pk = new HVEPublicKey(groupG, groupGT, groupG.getOrder(), e, g_q, _V, _A, theUHWMatrix);
	  
	  //Creating the HVEKey = (HVEPublicKey, HVESecretKey)
	  HVEKey key = new HVEKey(pk, sk);
	  
	  //Remember the key
	  usersKeys.put(userID, key);
	  
	  return key;
  }

   private Element[][] computeUHWMatrix(Element[][] listOfUHWTriplets, Element[][] listOfRTriplets, int l) {
	  Element UHWmatrix[][] = new Element[l][3];
	  
	  for (int i=0; i<l; i++) {
		  UHWmatrix[i][0] = listOfUHWTriplets[i][0].duplicate().mul(listOfRTriplets[i][0]);
		  UHWmatrix[i][1] = listOfUHWTriplets[i][1].duplicate().mul(listOfRTriplets[i][1]);
		  UHWmatrix[i][2] = listOfUHWTriplets[i][2].duplicate().mul(listOfRTriplets[i][2]);
	  }
	  
	  return UHWmatrix;
  }
  
  public HVEKey getKey(String id) {
	  HVEKey key = usersKeys.get(id);
	  if(key == null) {
		  throw new IllegalArgumentException("User ID not found");
	  }
	  return key;
  }

  public BigInteger getN() {
  return parameters.getBigInteger("n");
  }

  public Field getGroupG() {
  return groupG;
  }

  public Field getGroupGT() {
  return groupGT;
  }

  public BigInteger getLambda() {
  return lambda;
  }

  public Set<Element> getMessageSpaceM() {
  return messageSpaceM;
  }
  
  public PairingParameters getParameters() {
	return parameters;
}

public Pairing getE() {
	return e;
}
}