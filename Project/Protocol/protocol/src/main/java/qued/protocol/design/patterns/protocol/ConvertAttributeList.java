/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qued.protocol.design.patterns.protocol;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;

/**
 *
 * @author Alice
 */
public interface ConvertAttributeList {
    	public void convertFromRange(AttributeList initial_format, EqAttributeList final_format); 
        public void convertFromSubset(AttributeList initial_format, EqAttributeList final_format); 

}
