package qued.protocol.edu.si.qed.HVEprotocol.models;

import java.math.BigInteger;
import java.util.ArrayList;

public class EqAttributeList implements AttributeList{
	private Integer l = 0 ;
	private ArrayList<BigInteger> values = new ArrayList<BigInteger>();
	
	public EqAttributeList() {
	}
	
	public EqAttributeList(Integer l, ArrayList<BigInteger> values) {
		this.l = l;
		for (BigInteger item : values)         
                	this.values.add(item);
	}
	
	public Integer getL() {
		l = values.size();
		return l;
	}

	public ArrayList<BigInteger> getValues() {
		// TODO Auto-generated method stub
		return values;
	}
        
        public void setL(Integer l){
            this.l = l;                
        }

         public void setValues(ArrayList<BigInteger> values){
            for (BigInteger item : values) 
                this.values.add(item);
         }
         
}
