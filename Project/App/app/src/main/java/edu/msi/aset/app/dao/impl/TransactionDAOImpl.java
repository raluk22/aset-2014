package edu.msi.aset.app.dao.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.msi.aset.app.dao.TransactionDAO;
import edu.msi.aset.app.model.TransactionEntity;

@Service
@Transactional
public class TransactionDAOImpl extends AbstractDAOImpl<TransactionEntity> implements TransactionDAO{

	protected TransactionDAOImpl(Class<TransactionEntity> entityClass) {
		super(entityClass);
	}
	
	public TransactionDAOImpl() {
		super(TransactionEntity.class);
	}


}