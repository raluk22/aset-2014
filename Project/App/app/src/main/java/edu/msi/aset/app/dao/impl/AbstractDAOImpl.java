package edu.msi.aset.app.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.msi.aset.app.dao.AbstractDAO;

@Service
@Transactional
public abstract class AbstractDAOImpl<E> implements AbstractDAO<E> {


    private final Class<E> entityClass;

    protected AbstractDAOImpl(final Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    protected final Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public E findById(final int entityId) {
        return (E) getCurrentSession().get(this.entityClass, entityId);

    }

    @Override
    public void save(final E entityToBeSaved) {
        getCurrentSession().saveOrUpdate(entityToBeSaved);
        getCurrentSession().flush();

    }

    @Override
    public void update(final E entityToBeUpdated) {
        getCurrentSession().merge(entityToBeUpdated);
        getCurrentSession().flush();

    }

    @Override
    public void remove(final E entityToBeRemoved) {
        getCurrentSession().delete(getCurrentSession().merge(entityToBeRemoved));
        getCurrentSession().flush();

    }

    @SuppressWarnings("unchecked")
    @Override
    public E remove(final int entityToBeRemovedId) {
        final E entityToBeRemoved =
            (E) getCurrentSession().get(this.entityClass, entityToBeRemovedId);
        getCurrentSession().delete(entityToBeRemoved);
        getCurrentSession().flush();
        return entityToBeRemoved;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<E> findAll() {
        final Criteria criteria = getCurrentSession().createCriteria(this.entityClass);
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<E> findListByCriteria(final Criterion criterion) {
        final Criteria criteria = getCurrentSession().createCriteria(this.entityClass);
        criteria.add(criterion);
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public E findByCriteria(final Criterion criterion) {
        final Criteria criteria = getCurrentSession().createCriteria(this.entityClass);
        criteria.add(criterion);
        return (E) criteria.uniqueResult();
    }

}
