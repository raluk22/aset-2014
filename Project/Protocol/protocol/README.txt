In order to be able to use the libraries using maven please follow the next steps:

1. Install maven on your computer (if not already installed) using the following instruction:
	#Download maven from here: http://maven.apache.org/download.cgi
	#Unzip the distribution archive, i.e. apache-maven-3.2.3-bin.zip to the directory you wish to install Maven 3.2.3. These instructions assume you chose C:\Program Files\Apache Software Foundation. The subdirectory apache-maven-3.2.3 will be created from the archive.
	#Add the M2_HOME environment variable by opening up the system properties (WinKey + Pause), selecting the "Advanced" tab, and the "Environment Variables" button, then adding the M2_HOME variable in the user variables with the value C:\Program Files\Apache Software Foundation\apache-maven-3.2.3. Be sure to omit any quotation marks around the path even if it contains spaces. Note: For Maven 2.0.9, also be sure that the M2_HOME doesn't have a '\' as last character.
	#In the same dialog, add the M2 environment variable in the user variables with the value %M2_HOME%\bin.
	#Optional: In the same dialog, add the MAVEN_OPTS environment variable in the user variables to specify JVM properties, e.g. the value -Xms256m -Xmx512m. This environment variable can be used to supply extra options to Maven.
	#In the same dialog, update/create the Path environment variable in the user variables and prepend the value %M2% to add Maven available in the command line.
	#In the same dialog, make sure that JAVA_HOME exists in your user variables or in the system variables and it is set to the location of your JDK, e.g. C:\Program Files\Java\jdk1.7.0_51 and that %JAVA_HOME%\bin is in your Path environment variable.

2. Install the FlexiCore jars (already on SVN) on your local repository by running the following commands in cmd (replace 'your_path' accordingly)
	mvn install:install-file -X -Dfile="<your_path>\Project\Protocol\Libs\FlexiCoreProvider-1.7p7.signed.jar" -DgroupId="de.flexicore" -DartifactId="flexicore.provider" -Dversion=1.7 -Dpackaging="jar" -DgeneratePom=true 

	mvn install:install-file -Dfile="<your_path>\Project\Protocol\Libs\FlexiECProvider-1.7p7.signed.jar" -DgroupId="de.flexicore" -DartifactId="flexi.ec.provider" -Dversion=1.7 -Dpackaging="jar" -DgeneratePom=true 

	mvn install:install-file -Dfile="<your_path>\Project\Protocol\Libs\CoDec-build17-jdk13.jar" -DgroupId="de.flexicore" -DartifactId="codec" -Dversion=17 -Dpackaging="jar" -DgeneratePom=true 

	mvn install:install-file -Dfile="<your_path>\Project\Protocol\Libs\jpbc-api-2.0.0.jar" -DgroupId="JPBC" -DartifactId="JPBC_api" -Dversion=2.0.0 -Dpackaging="jar" -DgeneratePom=true 

	mvn install:install-file -Dfile="<your_path>\Project\Protocol\Libs\jpbc-crypto-2.0.0.jar" -DgroupId="JPBC" -DartifactId="JPBC_crypto" -Dversion=2.0.0 -Dpackaging="jar" -DgeneratePom=true 

	mvn install:install-file -Dfile="<your_path>\Project\Protocol\Libs\jpbc-pbc-2.0.0.jar" -DgroupId="JPBC" -DartifactId="JPBC_pbc" -Dversion=2.0.0 -Dpackaging="jar" -DgeneratePom=true 

	mvn install:install-file -Dfile="<your_path>\Project\Protocol\Libs\jpbc-plaf-2.0.0.jar" -DgroupId="JPBC" -DartifactId="JPBC_plaf" -Dversion=2.0.0 -Dpackaging="jar" -DgeneratePom=true 

	mvn install:install-file -Dfile="<your_path>\Project\Protocol\Libs\jna-3.2.5.jar" -DgroupId="JPBC" -DartifactId="jna" -Dversion=3.2.5 -Dpackaging="jar" -DgeneratePom=true 

	mvn install:install-file -Dfile="<your_path>\Project\Protocol\Libs\bcprov-jdk16-1.46.jar" -DgroupId="JPBC" -DartifactId="bcprov" -Dversion=1.46 -Dpackaging="jar" -DgeneratePom=true 

3. Run the app.