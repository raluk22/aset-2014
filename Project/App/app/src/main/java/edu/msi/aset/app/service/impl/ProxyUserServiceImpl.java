package edu.msi.aset.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.msi.aset.app.model.UserEntity;
import edu.msi.aset.app.security.RSAEncryptDecrypt;
import edu.msi.aset.app.service.ProxyUserService;

@Service
public class ProxyUserServiceImpl implements ProxyUserService{

	@Autowired
	private UserServiceImpl userServiceImpl;
	
	private RSAEncryptDecrypt enc = new RSAEncryptDecrypt();
	
	@Override
	public void getUser(String user) {
		userServiceImpl.getUser(user);
	}

	@Override
	public void saveUser(UserEntity user) {
		user.setPassword(enc.encrypt(user.getPassword()));
		userServiceImpl.saveUser(user);
	}

}
