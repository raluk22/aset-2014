package qued.protocols.edu.si.qed.services;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Field;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class MapToPoint {
	private HashMap<String, Element> vocab = new HashMap<String, Element>();
	
	private Field<Element> group;
	
	public MapToPoint() {		
	}
	
	public MapToPoint(Field<Element> group) {
		this.group = group;
	}
	
	public Element mapToPoint(String text) {
		Element result;
		
		if (vocab.containsKey(text))
			result = vocab.get(text);
		else
		{
			result = group.newRandomElement();
			vocab.put(text, result);
		}
			
		return result;
	}
	
	public String mapToString(Element m) {
		String result = null;
		
		Set<String> keys = vocab.keySet();
		
		Iterator<String> it = keys.iterator();
		while (it.hasNext()) {
			String k = it.next();
			if (vocab.get(k).equals(m))
			{
				result = k;
				break;
			}
		}
			
		return result;
	}
	
	public boolean isMInMessageSpace(Element M) {
		return vocab.containsValue(M);
	}
}
