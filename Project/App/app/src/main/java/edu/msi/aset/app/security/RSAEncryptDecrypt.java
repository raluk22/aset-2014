package edu.msi.aset.app.security;

import java.math.BigInteger;
import java.security.SecureRandom;

public class RSAEncryptDecrypt {

    private BigInteger n, d, e;

    private int bitlen = 1024;

    /**
     * Create an instance that can encrypt using someone elses public key.
     */
    public RSAEncryptDecrypt(BigInteger newn, BigInteger newe) {
        n = newn;
        e = newe;
    }

    public RSAEncryptDecrypt() {
        bitlen = 1024;
        SecureRandom r = new SecureRandom();
        BigInteger p = new BigInteger(bitlen / 2, 100, r);
        BigInteger q = new BigInteger(bitlen / 2, 100, r);
        n = p.multiply(q);
        BigInteger m = (p.subtract(BigInteger.ONE)).multiply(q
                .subtract(BigInteger.ONE));
        e = new BigInteger("3");
        while (m.gcd(e).intValue() > 1) {
            e = e.add(new BigInteger("2"));
        }
        d = e.modInverse(m);
    }

    public String encrypt(String message) {
        String encryptedMessage = new BigInteger(message.getBytes()).modPow(e, n).toString();
        return encryptedMessage;
    }

    public String decrypt(String message) {
        String decryptedMessage = new BigInteger(message).modPow(d, n).toString();
        return decryptedMessage;
    }
      /** Generate a new public and private key set. */
  public synchronized void generateKeys() {
    SecureRandom r = new SecureRandom();
    BigInteger p = new BigInteger(bitlen / 2, 100, r);
    BigInteger q = new BigInteger(bitlen / 2, 100, r);
    n = p.multiply(q);
    BigInteger m = (p.subtract(BigInteger.ONE)).multiply(q
        .subtract(BigInteger.ONE));
    e = new BigInteger("3");
    while (m.gcd(e).intValue() > 1) {
      e = e.add(new BigInteger("2"));
    }
    d = e.modInverse(m);
  }

  /** Return the modulus. */
  public synchronized BigInteger getN() {
    return n;
  }

  /** Return the public key. */
  public synchronized BigInteger getE() {
    return e;
  }

}
