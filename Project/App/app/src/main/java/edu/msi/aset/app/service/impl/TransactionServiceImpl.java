package edu.msi.aset.app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.msi.aset.app.builder.TransactionBuilder;
import edu.msi.aset.app.dao.TransactionDAO;
import edu.msi.aset.app.model.BeneficiaryEntity;
import edu.msi.aset.app.model.TransactionEntity;
import edu.msi.aset.app.model.UserEntity;
import edu.msi.aset.app.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService{
	
	@Autowired
	private TransactionDAO transactionDAO;


	@Override
	public List<TransactionEntity> getAllTransactions() {
		return getTrxs();
	}

	@Override
	public List<TransactionEntity> getTransactionByDate(Date from, Date to) {
		return getTrxs2();		
	}

	@Override
	public List<TransactionEntity> getTransactionByAmount(Integer from, Integer to) {
		return getTrxs();		
	}

	@Override
	public List<TransactionEntity> getTransactionByBeneficiary(String beneficiaryName) {
		return getTrxs();
	}

	@Override
	public List<TransactionEntity> getTransactionByKeyword(String keyword) {
		List<TransactionEntity> trxs = new ArrayList<TransactionEntity>();
		trxs.addAll(getTrxs());
		trxs.addAll(getTrxs2());
		return trxs;
	}

	@Override
	public void addTransaction(TransactionEntity transaction) {
	}
	
	private List<TransactionEntity> getTrxs() {
		BeneficiaryEntity ben = new BeneficiaryEntity();
		UserEntity user = new UserEntity();
		user.setName("Payee");
		ben.setIBAN("ROBTRL2874387329X");
		ben.setName("John Doe");
		TransactionBuilder trxBuilder = new TransactionBuilder();
		TransactionEntity trx = trxBuilder.withAmount(100.2)
				.withBeneficiary(ben)
				.withDescription("payments")
				.withTransactionDate(new Date())
				.withPayee(user)
				.build();
		List<TransactionEntity> trxs = new ArrayList<TransactionEntity>();	
		trxs.add(trx);
		return trxs;
	}
	
	private List<TransactionEntity> getTrxs2() {
		BeneficiaryEntity ben = new BeneficiaryEntity();
		UserEntity user = new UserEntity();
		user.setName("Payee");
		ben.setIBAN("ROBTRL2874387329X");
		ben.setName("Jane Doe");
		TransactionBuilder trxBuilder = new TransactionBuilder();
		TransactionEntity trx = trxBuilder.withAmount(100.2)
				.withBeneficiary(ben)
				.withDescription("payments")
				.withTransactionDate(new Date())
				.withPayee(user)
				.build();
		List<TransactionEntity> trxs = new ArrayList<TransactionEntity>();	
		trxs.add(trx);
		trxs.addAll(getTrxs());
		return trxs;
	}

	public TransactionDAO getTransactionDAO() {
		return transactionDAO;
	}

	public void setTransactionDAO(TransactionDAO transactionDAO) {
		this.transactionDAO = transactionDAO;
	}
}