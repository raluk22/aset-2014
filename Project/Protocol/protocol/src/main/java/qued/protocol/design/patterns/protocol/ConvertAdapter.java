/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qued.protocol.design.patterns.protocol;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;

/**
 *
 * @author Alice
 */
public class ConvertAdapter implements Convert {

    ConvertAttributeList convertAttributeList;

    public ConvertAdapter(String action) {
        if (action.equalsIgnoreCase("Encrypt")) {
            convertAttributeList = new ConvertForEncrypt();
        } else if (action.equalsIgnoreCase("GenToken")) {
            convertAttributeList = new ConvertForGenToken();
        }
    }

    public void convert(String action, AttributeList initial_format, EqAttributeList final_format) {

        if (action.equalsIgnoreCase("Encrypt")) {
            convertAttributeList.convertFromRange(initial_format, final_format);
        } else if (action.equalsIgnoreCase("GenToken")) {
            convertAttributeList = new ConvertForGenToken();
        }

    }

}
