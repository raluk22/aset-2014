
	<ion-view title="Transactions">
		<ion-content class="padding">
			<div class="list">
				<label class="item item-input item-stacked-label">
					<span class="input-label">Beneficiary:</span>
					{{beneficiary}}
				</label>
				<label class="item item-input item-stacked-label">
					<span class="input-label">Transaction date:</span>
					{{transactionDate}}
				</label>
				<label class="item item-input item-stacked-label">
					<span class="input-label">Amount:</span>
					{{amount}}
				</label>

				<label class="item item-input item-stacked-label">
					<span class="input-label">Description:</span>
					{{description}}
				</label>

				<label class="item item-input item-stacked-label">
					<span class="input-label">Payee:</span>
					{{payee}}
				</label>
			</div>
		</ion-content>
	</ion-view>