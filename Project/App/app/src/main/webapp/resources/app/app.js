angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.jsp"
    })
    
    .state('tab.home', {
      url: '/home',
      views: {
        'tab-home': {
          templateUrl: 'templates/tab-home.jsp',
          controller: 'HomeCtrl'
        }
      }
    })
    
    .state('tab.transactions', {
      url: '/transactions',
      views: {
        'tab-transactions': {
          templateUrl: 'templates/tab-transactions.jsp',
          controller: 'TransactionsCtrl'
        }
      }
    })	
    
    .state('login', {
        url: '/login',
        templateUrl: "templates/login.jsp",
        controller: 'LoginCtrl'
      });
    $urlRouterProvider.otherwise('/login');
});
