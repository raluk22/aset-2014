package qued.protocol.edu.si.qed.HVEprotocol.models;

public class HVEKey {

  private HVEPublicKey pK;

  private HVESecretKey sK;

  public HVEKey(HVEPublicKey pK, HVESecretKey sK) {
	super();
	this.pK = pK;
	this.sK = sK;
}

public HVEPublicKey getPK() {
  return pK;
  }

  public HVESecretKey getSK() {
  return sK;
  }

}