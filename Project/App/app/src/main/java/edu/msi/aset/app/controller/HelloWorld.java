package edu.msi.aset.app.controller;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import edu.msi.aset.app.service.TransactionService;

@Controller
public class HelloWorld {

	@Autowired
	private TransactionService transactionService;

	@RequestMapping(value = { "/home", "" })
	public ModelAndView home(HttpSession session,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("helloView");
		
		transactionService.addTransaction(null);
		model.addObject("trxListBeneficiary", transactionService.getTransactionByBeneficiary("benname"));
		model.addObject("trxListDate", transactionService.getTransactionByDate(new Date(), new Date()));
		model.addObject("trxListAmount", transactionService.getTransactionByAmount(1, 2));
		model.addObject("trxListKeyword", transactionService.getTransactionByKeyword("my trx"));
		model.addObject("trxListAll", transactionService.getAllTransactions());
	
		
		return model;
	}
	
	@RequestMapping(value = { "/test" })
	public ModelAndView test(HttpSession session,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("helloView");
		
		
		return model;
	}
}