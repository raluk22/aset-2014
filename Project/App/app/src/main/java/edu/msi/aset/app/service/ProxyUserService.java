package edu.msi.aset.app.service;

import edu.msi.aset.app.model.UserEntity;

public interface ProxyUserService {

	public void getUser(String user);

	public void saveUser(UserEntity user);
}
