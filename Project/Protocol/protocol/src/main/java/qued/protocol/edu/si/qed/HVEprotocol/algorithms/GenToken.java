package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.HVESecretKey;
import qued.protocol.edu.si.qed.HVEprotocol.models.Token;
import it.unisa.dia.gas.jpbc.Element;

public abstract class GenToken {

  public final Token genToken(HVESecretKey sK, AttributeList theI, int[] index, BigInteger p) {
	  
	  /* Primitive operation: changing the format of an attributes list to a format know by the genToken algorithm */
		EqAttributeList values = null;
		try {
			ArrayList<Integer> newIndex = new ArrayList<Integer>();
			values = convertAttributesValuesList(theI, newIndex);
			
			index = new int[newIndex.size()];
			int k=0;
			for (Integer i: newIndex) {
				index[k] = i;
				k++;
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  /* the implementation of GenTokenHVE*/
	  int l = index.length;
	  BigInteger r1, r2;
	  Element[][] theUHW = sK.getTheUHWMatrix();
	  Element v = sK.getV();
	  
	  Element K0 = sK.getG().duplicate().pow(sK.getAlpha());
	  Element[][] theKMatrix = new Element[values.getL()][2];
	  
	  for (int i=0; i<index.length; i++) {
		  r1 = new BigInteger(p.bitLength(), new Random()).mod(p);
		  r2 = new BigInteger(p.bitLength(), new Random()).mod(p);
		  
		  Element term1 = theUHW[index[i]][0].duplicate().pow(values.getValues().get(index[i])).pow(r1);
		  Element term2 = theUHW[index[i]][1].duplicate().pow(r1);
		  Element term3 = theUHW[index[i]][2].duplicate().pow(r2);
		 
		  K0 = K0.mul(term1).mul(term2).mul(term3);
		  theKMatrix[index[i]][0] = v.duplicate().pow(r1);
		  theKMatrix[index[i]][1] = v.duplicate().pow(r2);
	  }
	  
	  
	  Token res = new Token(theI, index, K0, theKMatrix);
	  return res;
  }

  public abstract EqAttributeList convertAttributesValuesList(AttributeList attributes, ArrayList<Integer> index) throws Exception;

}