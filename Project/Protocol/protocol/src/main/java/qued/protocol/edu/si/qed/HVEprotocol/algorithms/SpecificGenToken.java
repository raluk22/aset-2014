package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import java.util.ArrayList;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.RangeAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.SubsetAttributeList;

public class SpecificGenToken extends GenToken{

	private ConvertAttributeList conversionStrategy;
	
	@Override
	public EqAttributeList convertAttributesValuesList(AttributeList attributes, ArrayList<Integer> index) throws Exception {
		EqAttributeList res;
		
		if (!(attributes instanceof EqAttributeList)){
			res = new EqAttributeList();
			
			if (attributes instanceof SubsetAttributeList) {
				conversionStrategy = new FromSubsetConvertForGenToken();
			}
			else
				if (attributes instanceof RangeAttributeList) {
					conversionStrategy = new FromRangeConvertForGenToken();
				}
			
			conversionStrategy.convert(attributes, index, res);
		}
		else {
			res = (EqAttributeList) attributes;
		}
		
		return res;
	}

	/*
	public EqAttributeList convertAttributesValuesList(EqAttributeList attributes, ArrayList<Integer> index) throws Exception {
		return attributes;
	}
	
	public EqAttributeList convertAttributesValuesList(RangeAttributeList attributes, ArrayList<Integer> index) throws Exception {
		EqAttributeList res;
		res = new EqAttributeList();
		conversionStrategy = new FromRangeConvertForGenToken();
		conversionStrategy.convert(attributes, index, res);
		
		return res;
	}
	
	public EqAttributeList convertAttributesValuesList(SubsetAttributeList attributes, ArrayList<Integer> index) throws Exception {
		EqAttributeList res;
		res = new EqAttributeList();
		conversionStrategy = new FromSubsetConvertForGenToken();
		conversionStrategy.convert(attributes, index, res);
		
		return res;
	}
	
	*/
}
