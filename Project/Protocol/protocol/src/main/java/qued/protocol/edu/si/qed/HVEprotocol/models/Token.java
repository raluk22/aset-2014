package qued.protocol.edu.si.qed.HVEprotocol.models;

import it.unisa.dia.gas.jpbc.Element;

public class Token {

  private AttributeList attributesValuesListI;
  
  private int[] index;

  private Element pointK0;

  private Element[][] theKMatrix;

  public Token(AttributeList attributesValuesListI, int[] index, Element pointK0,
		Element[][] theKMatrix) {
	super();
	this.attributesValuesListI = attributesValuesListI;
	this.index = index;
	this.pointK0 = pointK0;
	this.theKMatrix = theKMatrix;
}

public AttributeList getAttributesValuesListI() {
	return attributesValuesListI;
}

public Element getPointK0() {
	return pointK0;
}

public Element[][] getTheKMatrix() {
	return theKMatrix;
}

public void setAttributesValuesListI(AttributeList attributesValuesListI) {
	this.attributesValuesListI = attributesValuesListI;
}

public void setPointK0(Element pointK0) {
	this.pointK0 = pointK0;
}

public void setTheKMatrix(Element[][] theKMatrix) {
	this.theKMatrix = theKMatrix;
}

public int[] getIndex() {
	return index;
}

public void setIndex(int[] index) {
	this.index = index;
}
}