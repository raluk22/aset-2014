package qued.protocol.edu.si.qed.aspect;

import java.util.Stack;

import org.aspectj.lang.JoinPoint;

public aspect ExceptionTracingAspect {

		Stack<String> calls;
		
		pointcut methodCall(): call (* *.*(..)) && !within(ExceptionTracingAspect) && !within(qued.protocol.EncryptQueryTest);
		
		pointcut methodExecution(): execution (* *.*(..)) && !within(ExceptionTracingAspect) && !within(qued.protocol.EncryptQueryTest);
		
		ExceptionTracingAspect() {
			calls = new Stack<String>();
		}
		
		before(): methodCall() {
			calls.push(thisJoinPoint.getSignature().toLongString());
		}
		
		Object around() : methodExecution()  {  
		     try{  
		         return proceed();  
		     } catch(Throwable th){  
		         print(th, thisJoinPoint);
		         return th.getMessage();  
		     }  
		}
     
		after(): methodCall() {
			if (!calls.isEmpty())
				calls.pop();
		}
		
		private void print(Throwable e, JoinPoint jp) {
			System.out.println("\n *** The exception "+ e.getClass().getName()+" ; "+ e.getLocalizedMessage()+" has been thrown at");
			System.out.println("\n >> "+jp.getSignature().toLongString());
			StringBuilder spaces= new StringBuilder(" ");
			
			while (!calls.isEmpty())
			{
				spaces.append("  ");
				System.out.println(spaces+ " >> "+calls.pop());
			}
		}
}
