package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Field;
import it.unisa.dia.gas.jpbc.PairingParameters;

import java.math.BigInteger;
import java.util.Random;

public class BilinearGroupUtils {

	 public static Element getGeneratorForGp(Field<?> groupG, PairingParameters parameters) {
		 
		  Element g = groupG.newRandomElement();
		  Element g_p = g.pow(parameters.getBigInteger("n1"));
		  //System.out.println("the generator for Gp: "+g_p);
		  //System.out.println("is order p?: "+g_p.pow(parameters.getBigInteger("n0")).isOne());
		  return g_p;
	  }
	  
	 public static Element getGeneratorForGq(Field<?> groupG, PairingParameters parameters) {
		  Element g = groupG.newRandomElement();
		  Element g_q = g.pow(parameters.getBigInteger("n0"));
		  //System.out.println("the generator for Gq: "+g_q);
		  //System.out.println("is order q?: "+g_q.pow(parameters.getBigInteger("n1")).isOne());
		  
		  return g_q;
	  }
	  
	 public static Element getRandomFromSubgroupOfG(Element gen, PairingParameters parameters) {
		  Random rand = new Random();
		  BigInteger rnd_int = new BigInteger(parameters.getBigInteger("n").bitLength(), rand).mod(parameters.getBigInteger("n")); 
		 
		  Element g = gen.duplicate();
		  g = g.pow(rnd_int);
		  ////System.out.println("g random din subgroup = " +gen);
		  
		  return g;
	  }
	  
	 public static Element getRandomFromSubgroupOfG(Element gen, BigInteger n) {
		  Random rand = new Random();
		  BigInteger rnd_int = new BigInteger(n.bitLength(), rand).mod(n); 
		  Element g = gen.duplicate();
		  g = g.pow(rnd_int);
		 
		  return g;
	  }
	 
	 public static Element[][] generateTriplets(int l, Element g_p, PairingParameters parameters) {
		  Element listOfUHWTriplets[][] = new Element[l][3];

		  for (int i=0; i<l; i++) {
			  listOfUHWTriplets[i][0] = getRandomFromSubgroupOfG(g_p, parameters);
			  listOfUHWTriplets[i][1] = getRandomFromSubgroupOfG(g_p, parameters);
			  listOfUHWTriplets[i][2] = getRandomFromSubgroupOfG(g_p, parameters);
		  }
		  return listOfUHWTriplets;
	  }
	  
}
