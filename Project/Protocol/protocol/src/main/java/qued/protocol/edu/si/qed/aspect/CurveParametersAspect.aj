package qued.protocol.edu.si.qed.aspect;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Setup;
import qued.protocol.edu.si.qed.HVEprotocol.models.*;

public aspect CurveParametersAspect {
	
	FileOutputStream fout;
	
	pointcut parametersGeneration(): call (* Setup.initialize(..));
	
	pointcut curve(): call (* *.generate()) && within(Setup);
	
	public CurveParametersAspect() throws FileNotFoundException{
		fout = new FileOutputStream("D:/Facultate/An 1/Semestrul 1/TAIP/proiect/aset-2014/Project/Protocol/protocol/src/main/resources/ECC_Parameters.txt");
	}
	
	before(): curve() {
		String record = "<< Parameters for the system >> " +
				" \n 	--> The curve generator used : "+ thisJoinPoint.getTarget().getClass().getCanonicalName();
				
		try {
			fout.write(record.getBytes());
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}					
	}
	
	after(): parametersGeneration() {
		String record = " \n 	--> The pairing parameters : "+
						" \n 		--> p = "+((Setup) thisJoinPoint.getTarget()).getParameters().getBigInteger("n0") +
						" \n 		--> q = "+((Setup) thisJoinPoint.getTarget()).getParameters().getBigInteger("n1");
		try {
			fout.write(record.getBytes());
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}					
	}

}
