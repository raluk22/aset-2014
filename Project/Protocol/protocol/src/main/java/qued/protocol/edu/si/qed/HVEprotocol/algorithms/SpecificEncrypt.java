package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.RangeAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.SubsetAttributeList;

public class SpecificEncrypt extends Encrypt{

	private ConvertAttributeList conversionStrategy;
	
	@Override
	public EqAttributeList convertAttributesValuesList(AttributeList attributes) throws Exception {
		EqAttributeList res;
		
		if (!(attributes instanceof EqAttributeList)){
			res = new EqAttributeList();
			
			if (attributes instanceof SubsetAttributeList) {
				conversionStrategy = new FromSubsetConvertForEncrypt();
			}
			else
				if (attributes instanceof RangeAttributeList) {
					conversionStrategy = new FromRangeConvertForEncrypt();
				}
			
			conversionStrategy.convert(attributes, null, res);
		}
		else {
			res = (EqAttributeList) attributes;
		}
		
		return res;
	}

	public EqAttributeList convertAttributesValuesList(EqAttributeList attributes) throws Exception {		
		return attributes;
	}
	
	public EqAttributeList convertAttributesValuesList(RangeAttributeList attributes) throws Exception {
		EqAttributeList res;
		
		res = new EqAttributeList();
		conversionStrategy = new FromRangeConvertForEncrypt();
		conversionStrategy.convert(attributes, null, res);
		
		return res;
	}
	
	public EqAttributeList convertAttributesValuesList(SubsetAttributeList attributes) throws Exception {
		EqAttributeList res;
		
		res = new EqAttributeList();
		conversionStrategy = new FromSubsetConvertForEncrypt();
		conversionStrategy.convert(attributes, null, res);
		
		return res;
	}
}
