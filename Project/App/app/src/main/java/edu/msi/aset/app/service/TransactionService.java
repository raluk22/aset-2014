package edu.msi.aset.app.service;

import java.util.Date;
import java.util.List;

import edu.msi.aset.app.model.TransactionEntity;

public interface TransactionService {
	
  public List<TransactionEntity> getAllTransactions();

  public List<TransactionEntity> getTransactionByDate(Date from,  Date to);

  public List<TransactionEntity> getTransactionByAmount( Integer from,  Integer to);

  public List<TransactionEntity> getTransactionByBeneficiary(String beneficiary);

  public List<TransactionEntity> getTransactionByKeyword(String keyword);

  public void addTransaction(TransactionEntity transaction);

}