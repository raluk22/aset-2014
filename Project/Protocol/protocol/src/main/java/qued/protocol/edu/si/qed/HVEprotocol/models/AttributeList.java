package qued.protocol.edu.si.qed.HVEprotocol.models;
import java.math.BigInteger;
import java.util.ArrayList;

public interface AttributeList {

  public Integer getL();

  public ArrayList<BigInteger> getValues();
  
  public void setL(Integer l);

  public void setValues(ArrayList<BigInteger> values);
  
}