package edu.msi.aset.app.model;

import java.util.Date;

public class TransactionEntity {

	private Double amount;
	private Date transactionDate;
	private String description;
	private BeneficiaryEntity beneficiary;
	private UserEntity payee;
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public BeneficiaryEntity getBeneficiary() {
		return beneficiary;
	}
	public void setBeneficiary(BeneficiaryEntity beneficiary) {
		this.beneficiary = beneficiary;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public UserEntity getPayee() {
		return payee;
	}
	public void setPayee(UserEntity payee) {
		this.payee = payee;
	}
	
	

}