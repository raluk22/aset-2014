angular.module('starter.controllers', [])
.controller('TransactionsCtrl', function($scope){
    $scope.amount = '100';
    $scope.transactionDate = '12.10.2014';
    $scope.description = 'safe';
    $scope.beneficiary = 'Iulia';
    $scope.payee = '';
	})
.controller('AccountCtrl', function($scope) {
	})
.controller('LoginCtrl', ['$scope', '$state', function($scope, $state){
  $scope.login = function() {
    $state.go('tab.home');
  };
}]);
