package edu.msi.aset.app.factory;

public abstract class Card {
	public abstract String cardType();
	public abstract String getType();
}
