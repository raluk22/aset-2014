package qued.protocol.edu.si.qed.HVEprotocol.models;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import it.unisa.dia.gas.jpbc.Element;

public class Ciphertext implements Serializable{

  private Element pointC0;

  private Element pointC1;

  private Element theCMatrix[][];

  public Ciphertext(Element pointC0, Element pointC1, Element[][] theCMatrix) {
	super();
	this.pointC0 = pointC0;
	this.pointC1 = pointC1;
	this.theCMatrix = theCMatrix;
}

  public Element getPointC0() {
  return pointC0;
  }

  public Element getPointC1() {
  return pointC1;
  }

  public Element[][] getTheCMatrix() {
	  return theCMatrix;
  }

  public static String writeObject(Ciphertext c) {
	  String serialized = "";
	  
	  try {
		     ByteArrayOutputStream bo = new ByteArrayOutputStream();
		     ObjectOutputStream so = new ObjectOutputStream(bo);
		     so.writeObject(c);
		     so.flush();
		     serialized = bo.toString();
		 } catch (Exception e) {
		     System.out.println(e);
		 }
	  
	return serialized;
  }
  
  public static Ciphertext readObject(String serialized) {
	  Ciphertext obj = null;
	  
	  try {
		     byte b[] = serialized.getBytes(); 
		     ByteArrayInputStream bi = new ByteArrayInputStream(b);
		     ObjectInputStream si = new ObjectInputStream(bi);
		     obj = (Ciphertext) si.readObject();
		 } catch (Exception e) {
		     System.out.println(e);
		 }
	  
	  return obj;
  }
}