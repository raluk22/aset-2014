package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import java.math.BigInteger;
import java.util.Random;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.Ciphertext;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.HVEPublicKey;
import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Field;

public abstract class Encrypt {

  public final Ciphertext encrypt(HVEPublicKey pK, AttributeList attributesValuesListI, Element M) {
	  /* Primitive operation: changing the format of an attributes list to a format know by the encrypt algorithm */
	  EqAttributeList values=null;
	try {
		values = convertAttributesValuesList(attributesValuesListI);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  /* the implementation of EncryptHVE*/
	  Field groupG = pK.getGroupG();
	  Field groupGT = pK.getGroupGT();
	  BigInteger n = groupG.getOrder();
	  
	  int l = values.getL();
	  Element g_q = pK.getG_q().duplicate();
	  
	  //Generate random s from Zn
	  BigInteger s = new BigInteger(n.bitLength(), new Random()).mod(n);
	  
	  Element _Z;
	  Element[][] listOfZ = new Element[l][2];
	  
	  //Picks randoms Z an (Zi_1, Zi_2) from Gq with i=1..l
	  _Z = BilinearGroupUtils.getRandomFromSubgroupOfG(g_q, n);
	  
	  for (int i=0; i<l; i++) {
		  listOfZ[i][0] = BilinearGroupUtils.getRandomFromSubgroupOfG(g_q, n);
		  listOfZ[i][1] = BilinearGroupUtils.getRandomFromSubgroupOfG(g_q, n);
	  }
	  
	  //Converts the string message to an element on the curve
	  //Element M = Utils.fromStringToElement(pK.getGroupGT(), messageM);
	  //System.out.println("The message M (as a point on the curve) before encrypt: "+ M);
	
	  //Computes C'=MA^s
	  Element _C = pK.getPointA().duplicate().pow(s);
	  Element _M = M.duplicate();
	  _C = _M.mul(_C);
	   
	  //Computes C0 = V^sZ
	  Element _C0 = pK.getPointV().duplicate().pow(s);
	  _C0 = (_C0).mul(_Z);
	 
	  //Computes matrix of C
	  Element[][] theCMatrix = new Element[l][2];
	  Element[][] theUHWMatrix = pK.getTheUHWMatrix();
	  
	  for (int i=0; i<l; i++) {
		Element term1 = theUHWMatrix[i][0].duplicate().pow(values.getValues().get(i)).pow(s);
		Element term2 = theUHWMatrix[i][1].duplicate().pow(s).mul(listOfZ[i][0]);
		theCMatrix[i][0] = term1.mul(term2);   
		
		term1 = theUHWMatrix[i][2].duplicate().pow(s);
		theCMatrix[i][1] = term1.mul(listOfZ[i][1]);   
	  }
	  
	  //The ciphertext
	  Ciphertext res = new Ciphertext(_C, _C0, theCMatrix);
	  
	  return res;
  }

  public abstract EqAttributeList convertAttributesValuesList(AttributeList attributes) throws Exception;
  
}

