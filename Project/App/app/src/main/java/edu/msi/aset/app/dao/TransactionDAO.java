package edu.msi.aset.app.dao;

import edu.msi.aset.app.model.TransactionEntity;


public interface TransactionDAO extends AbstractDAO<TransactionEntity> {
}