package qued.protocol;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.Random;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Field;

import org.junit.Before;
import org.junit.Test;

import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Setup;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Utils;

public class UtilsTest {

	private Setup setup;

	@Before
	public void setup() {
		BigInteger lambda = new BigInteger(100, (new Random()));
		
		setup = new Setup(lambda, 124);
	}
	
	@Test
	public void pointOnCurve() {
		Element M = Utils.fromStringToElement(setup.getGroupGT(), "Testing");
		Element Zero = setup.getGroupGT().newZeroElement();
		assertTrue(M.pow(setup.getGroupGT().getOrder()).equals(Zero));
	}

}
