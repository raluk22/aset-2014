package qued.protocols.edu.si.qed.services;

import it.unisa.dia.gas.jpbc.Element;

import java.math.BigInteger;
import java.util.Random;

import qued.protocol.edu.si.qed.HVEprotocol.algorithms.GenToken;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.Setup;
import qued.protocol.edu.si.qed.HVEprotocol.algorithms.SpecificGenToken;
import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.HVEKey;
import qued.protocol.edu.si.qed.HVEprotocol.models.HVESecretKey;
import qued.protocol.edu.si.qed.HVEprotocol.models.Token;

public class TrustedAuthority {

	private Setup setup;
	private GenToken tokenGenerator;
	private MapToPoint convertor;

	public TrustedAuthority() {
		BigInteger lambda = new BigInteger(100, (new Random()));
		setup = new Setup(lambda, 124);
		tokenGenerator = new SpecificGenToken();
		convertor = new MapToPoint(setup.getGroupGT());
	}
	
	public HVEKey generateNewKey(String userID, int newL) {
		return setup.genKey(userID, newL);
	}
	
	public Token generateToken(HVESecretKey sK, AttributeList theI, int[] index) {
		return tokenGenerator.genToken(sK, theI, index, setup.getParameters().getBigInteger("n0"));
	}
	
	 public HVEKey getKey(String id) {
		 return setup.getKey(id);
	 }
	 
	 public boolean isMInMessageSpace(Element mess) {
		 return convertor.isMInMessageSpace(mess);
	 }
	 
	 public Element mapToPoint(String text) {
		 return convertor.mapToPoint(text);
	 }
	 
	 public String mapToString(Element e) {
		 return convertor.mapToString(e);
	 }
}
