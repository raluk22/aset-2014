package qued.protocol.edu.si.qed.HVEprotocol.models;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Field;
import it.unisa.dia.gas.jpbc.Pairing;

import java.math.BigInteger;

public class HVEPublicKey {

  private Field groupG;
  
  private Field groupGT;
  
  private BigInteger n;
  
  private Pairing e;

  private Element g_q;

  private Element pointV;

  private Element pointA;

  private Element[][] theUHWMatrix;

  
	public Field getGroupG() {
		return groupG;
	}

	public Field getGroupGT() {
		return groupGT;
	}

	public BigInteger getN() {
		return n;
	}

	public Pairing getE() {
		return e;
	}

	public Element getG_q() {
		return g_q;
	}

	public Element getPointV() {
		return pointV;
	}

	public Element getPointA() {
		return pointA;
	}

	public Element[][] getTheUHWMatrix() {
		return theUHWMatrix;
	}
	
	public HVEPublicKey(Field groupG, Field groupGT, BigInteger n, Pairing e,
				Element g_q, Element pointV, Element pointA,
				Element[][] theUHWMatrix) {
			super();
			this.groupG = groupG;
			this.groupGT = groupGT;
			this.n = n;
			this.e = e;
			this.g_q = g_q;
			this.pointV = pointV;
			this.pointA = pointA;
			this.theUHWMatrix = theUHWMatrix;
		}
}