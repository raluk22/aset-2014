package qued.protocol.edu.si.qed.aspect;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import it.unisa.dia.gas.jpbc.Element;

public aspect MessageMappingAspect {

	FileOutputStream fout;
	
	pointcut mapping(): (execution (* *.fromStringToElement(..)));

	public MessageMappingAspect() throws FileNotFoundException{
		fout = new FileOutputStream("D:/Facultate/An 1/Semestrul 1/TAIP/proiect/aset-2014/Project/Protocol/protocol/src/main/resources/MessageMappingToElementsOfEC.txt");
	}
	
	Element around(): mapping() {
		Object[] args = thisJoinPoint.getArgs();
		
		try {
			String m = "	-- The message '"+ args[1] +"' is mapped to E ";
			fout.write(m.getBytes());	
		}
		catch (IOException ex) {
			System.out.println(ex.getMessage());
		}	
		
		Element e = proceed();
		
		try {
			fout.write((e.toString()+"\n").getBytes());
		}
		catch (IOException ex) {
			System.out.println(ex.getMessage());
		}	
		
		return e;
	}
}
