package edu.msi.aset.app.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

	@RequestMapping(value = { "/doLogin" })
	public ModelAndView doLogin(HttpSession session,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		return model;
	}

}