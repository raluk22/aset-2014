<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>QoED</title>
</head>
<body>
	<h3>By beneficiary</h3>
<c:forEach var="trx" begin="0" items="${trxListBeneficiary}">
	<hr/>
	<p>Amount: ${trx.amount}</p>
	<p>Date: ${trx.transactionDate}</p>
	<p>Description: ${trx.description}</p>
	<p>Beneficiary: ${trx.beneficiary.name}</p>
</c:forEach>
<h3>By date</h3>
<c:forEach var="trx" begin="0" items="${trxListDate}">
	<hr/>
	<p>Amount: ${trx.amount}</p>
	<p>Date: ${trx.transactionDate}</p>
	<p>Description: ${trx.description}</p>
	<p>Beneficiary: ${trx.beneficiary.name}</p>
</c:forEach>
<h3>By amount</h3>
<c:forEach var="trx" begin="0" items="${trxListAmount}">
	<hr/>
	<p>Amount: ${trx.amount}</p>
	<p>Date: ${trx.transactionDate}</p>
	<p>Description: ${trx.description}</p>
	<p>Beneficiary: ${trx.beneficiary.name}</p>
</c:forEach>
<h3>By keyword</h3>
<c:forEach var="trx" begin="0" items="${trxListKeyword}">
	<hr/>
	<p>Amount: ${trx.amount}</p>
	<p>Date: ${trx.transactionDate}</p>
	<p>Description: ${trx.description}</p>
	<p>Beneficiary: ${trx.beneficiary.name}</p>
</c:forEach>
<h3>All</h3>
<c:forEach var="trx" begin="0" items="${trxListAll}">
<hr/>
	<p>Amount: ${trx.amount}</p>
	<p>Date: ${trx.transactionDate}</p>
	<p>Description: ${trx.description}</p>
	<p>Beneficiary: ${trx.beneficiary.name}</p>
</c:forEach>
	
</body>
</html>