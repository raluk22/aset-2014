package edu.msi.aset.app.model;

public class BeneficiaryEntity {
	
	private String name;
	private String IBAN;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIBAN() {
		return IBAN;
	}
	public void setIBAN(String iBAN) {
		IBAN = iBAN;
	}
}
