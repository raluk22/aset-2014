package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Set;

import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.SubsetAttributeList;

public class FromSubsetConvertForGenToken implements ConvertAttributeList{

	public void convert(AttributeList initial_format, ArrayList<Integer> index,
			EqAttributeList final_format) throws Exception {
		ArrayList<Set<BigInteger>> values = ((SubsetAttributeList) initial_format).getValuesSet();
		
		Set<BigInteger> alphabet = ((SubsetAttributeList) initial_format).getT();
		int n = alphabet.size();
			
		int i=0;
		
		for (Set<BigInteger> set: values) {		
			if (set!=null) {
				int j =0;
				
				for (BigInteger el: alphabet) {
					if (!set.contains(el)) {
						
						final_format.getValues().add(i*n+j, new BigInteger("1"));
						index.add(i*n+j); 
					}
					else 
						final_format.getValues().add(i*n+j, null);
					j++;
				}
			}			
			i++;
		}
		
	}
}
