/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qued.protocol.edu.si.qed.HVEprotocol;

import java.math.BigInteger;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import qued.protocol.edu.si.qed.HVEprotocol.algorithms.FromRangeConvertForEncrypt;
import qued.protocol.edu.si.qed.HVEprotocol.models.AttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.EqAttributeList;
import qued.protocol.edu.si.qed.HVEprotocol.models.RangeAttributeList;
import static org.junit.Assert.*;

public class FromRangeConvertForEncryptTest {

    public FromRangeConvertForEncryptTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getN method, of class FromRangeConvertForEncrypt.
     */
    @Test
    public void testGetN() {
        System.out.println("getN");
        FromRangeConvertForEncrypt instance = new FromRangeConvertForEncrypt(2);
        Integer expResult = 2;
        Integer result = instance.getN();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of convert method, of class FromRangeConvertForEncrypt.
     */
    @Test
    public void testConvert() {
        System.out.println("convert");
        AttributeList initialFormat = new RangeAttributeList();
        initialFormat.setL(2);
        ArrayList<BigInteger> S = new ArrayList<BigInteger>();
        S.add(BigInteger.valueOf(3));
        S.add(BigInteger.valueOf(5));
        initialFormat.setValues(S);

        EqAttributeList finalFormat = new EqAttributeList();
        FromRangeConvertForEncrypt instance = new FromRangeConvertForEncrypt(7);
        instance.convert(initialFormat, finalFormat);

        //expectet result for example : w=2, S(3,5) and n =7 
        //sigma(s) = 1111000 | 1111110 => 11110001111110
        ArrayList<BigInteger> expectedResult = new ArrayList<BigInteger>();
        for (int i = 1; i <= 4; i++) {
            expectedResult.add(BigInteger.ONE);
        }
        for (int i = 1; i <= 3; i++) {
            expectedResult.add(BigInteger.ZERO);
        }
        for (int i = 1; i <= 6; i++) {
            expectedResult.add(BigInteger.ONE);
        }
        for (int i = 1; i <= 1; i++) {
            expectedResult.add(BigInteger.ZERO);
        }

        ArrayList<BigInteger> result = new ArrayList<BigInteger>();
        result = finalFormat.getValues();
        assertArrayEquals(expectedResult.toArray(), result.toArray());
    }

}
