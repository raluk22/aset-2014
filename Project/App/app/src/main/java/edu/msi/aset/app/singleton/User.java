package edu.msi.aset.app.singleton;

public class User {

	private static User user = null;

	private User() {
	}

	public static User getInstance() {
		if (user == null) {
			user = new User();
		}
		return user;
	}

	public void sayHello() {
		System.out.println("Welcome");
	}
	   
}
