package edu.msi.aset.app.factory;

public class FactoryDemo {

	public static void main(String[] args) {
		CardFactory cardFactory = new CardFactory();

		Card a1 = cardFactory.getType("credit");
		System.out.println("It is a " + a1.cardType() + " card");
		

		Card a2 = cardFactory.getType("debit");
		System.out.println("It is a " + a2.cardType() + " card");
	}


}
