package qued.protocol.edu.si.qed.HVEprotocol.algorithms;

import qued.protocol.edu.si.qed.HVEprotocol.models.Ciphertext;
import qued.protocol.edu.si.qed.HVEprotocol.models.Token;
import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Pairing;

public class Query {

  public static Element query(Token tK, Ciphertext c, Pairing e) {
	  int[] index = tK.getIndex();
	  Element[][] Cs = c.getTheCMatrix();
	  Element[][] Ks = tK.getTheKMatrix();
	  
	  // M = C'
	  Element M = c.getPointC0().duplicate();
	  
	  Element div = e.pairing(c.getPointC1(), tK.getPointK0());
	  Element div2 = e.getGT().newOneElement();
	  
	  for (int i=0; i<index.length; i++) {
		  div2 = div2.mul(e.pairing(Cs[index[i]][0], Ks[index[i]][0]).mul(e.pairing(Cs[index[i]][1], Ks[index[i]][1])));
	  }
	  
	  div = div.div(div2);
	  M = M.div(div);
	  return M;
  }

}