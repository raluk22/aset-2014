package qued.protocol.edu.si.qed.models;

import java.util.Date;

public class TransactionEntity {

	private String senderId;
	private String senderName;
	private String beneficiaryID;
	private String beneficiaryName;
	private String beneficiaryIBAN;
	private Double amount;
	private String currency;
	private Date transactionDate;
	private String description;
	
	public TransactionEntity() {}

	public TransactionEntity(String senderId, String senderName,
			String beneficiaryID, String beneficiaryName,
			String beneficiaryIBAN, Double amount, String currency,
			Date transactionDate, String description) {
		super();
		this.senderId = senderId;
		this.senderName = senderName;
		this.beneficiaryID = beneficiaryID;
		this.beneficiaryName = beneficiaryName;
		this.beneficiaryIBAN = beneficiaryIBAN;
		this.amount = amount;
		this.currency = currency;
		this.transactionDate = transactionDate;
		this.description = description;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getBeneficiaryID() {
		return beneficiaryID;
	}

	public void setBeneficiaryID(String beneficiaryID) {
		this.beneficiaryID = beneficiaryID;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryIBAN() {
		return beneficiaryIBAN;
	}

	public void setBeneficiaryIBAN(String beneficiaryIBAN) {
		this.beneficiaryIBAN = beneficiaryIBAN;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		StringBuffer result = new StringBuffer();
		
		result.append("Transaction: "+ " \n Sender ID = "+this.senderId+
				" \n Sender name = "+this.senderName+
				" \n Beneficiary ID = "+this.beneficiaryID+
				" \n Beneficiary name = "+this.beneficiaryName+
				" \n Beneficiary IBAN = "+this.beneficiaryIBAN+
				" \n Amount = "+this.amount+
				" \n Currency = "+this.currency+
				" \n Transaction date = "+this.transactionDate+
				" \n Description = "+this.description);
		
		return result.toString();
	}
}