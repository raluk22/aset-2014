package edu.msi.aset.app.aspect;

import java.util.Date;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import edu.msi.aset.app.model.TransactionEntity;

@Aspect
public class TransactionsAspect {

	@Before("execution(* edu.msi.aset.app.service.impl.TransactionServiceImpl.get*(..))")
	public void logBefore(JoinPoint joinPoint) {

		System.out.print(new Date() + ":");
		System.out.println("Transactions were queried.");
		System.out.print("Query : "
				+ joinPoint.getSignature().getName());
		System.out.print(" with params ");
		for (Object param : joinPoint.getArgs())
			System.out.print(": '" + toString(param) + "' ");
		System.out.println("\n******");
	}

	@SuppressWarnings("unchecked")
	@AfterReturning(pointcut = "execution(* edu.msi.aset.app.service.impl.TransactionServiceImpl.get*(..))", returning = "result")
	public void afterLog(JoinPoint jp, Object result) {
		System.out.print(new Date() + ":");
		System.out.println("Transaction query results");
		System.out.println("Query : " + jp.getSignature());
		System.out.println("Number of transactions returned:  "
				+ ((List<TransactionEntity>) result).size());
		System.out.println("\n******");
	}
	
	
	@AfterReturning(pointcut = "execution(* edu.msi.aset.app.service.impl.TransactionServiceImpl.add*(..))", returning = "result")
	public void afterAdd(JoinPoint jp, Object result) {
		System.out.print(new Date() + ":");
		System.out.println("Transaction added succesfully");
		System.out.println("\n******");
	}
	
	private String toString(Object obj) {
		String str = null;
		
		if (obj instanceof String) {
			str = (String) obj;
		}
		
		if (obj instanceof Date) {
			Date d = (Date) obj;
			str = d.toString();
		}
		
		if (obj instanceof Integer) {
			Integer i = (Integer) obj;
			str = String.valueOf(i);
		}
		
		return str;
	}
}
