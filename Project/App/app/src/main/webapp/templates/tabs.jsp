<ion-tabs class="tabs-icon-top">

  <!-- Home Tab -->
  <ion-tab title="Home" icon="icon ion-home" href="#/tab/home">
    <ion-nav-view name="tab-home"></ion-nav-view>
  </ion-tab>

  <!-- Account Tab -->
  <ion-tab title="Account" icon="icon ion-person" href="#/tab/account">
    <ion-nav-view name="tab-account"></ion-nav-view>
  </ion-tab>
  
  <!-- Transactions Tab -->
	<ion-tab title="Transactions" icon="icon ion-earth" href="#/tab/transactions">
		<ion-nav-view name="tab-transactions"></ion-nav-view>
	</ion-tab>

</ion-tabs>
